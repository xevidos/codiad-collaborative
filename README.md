# Collaborative

This plugin adds collaborative functionallity to Codiad by utilizing a socket.io node socket.

# Warnings

- When dealing with collaboration, there are always risks of loosing data.  Please make sure you back up your files regularly!

# Installation

- Install Node.JS
- Install Socket.io
- Download the zip file and extract it to your plugins folder
- Rename the config.default.json to config.json and replace the values in the file.
- Forward a port of your choosing and change the default port in the config.json
- Run sudo node /path/to/server.js
- If you would like to run the server at boot you can create a service that runs the command /usr/bin/node /path/to/server.js > /path/to/logfile.txt 2>&1

# Bugs

- If the user switches their file before joining is complete, the editor is blank and file must be closed without saving changes to restore it.  

# To Do

- Add in a backup of files so the user can choose between backup or saved copy if an error occurs during joining.

# Default Config

	{
		"address":"0.0.0.0",
		"port":"1337",
		"server":"https://myserver.com",
		"ssl":{
			"key":"/path/to/key.key",
			"certificate":"/path/to/cert.crt",
			"certificate_authority":"/path/to/chain.crt"
			
		}
	}

# Example Service
	[Unit]
	Description=Codaid Collaborative
	
	[Service]
	ExecStart=/bin/sh -c 'chmod +x /path/to/codiad/plugins/codiad-collaborative-master/start_server.sh;/path/to/codiad/plugins/codiad-collaborative-master/start_server.sh'
	
	[Install]
	WantedBy=multi-user.target