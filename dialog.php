<?php

/*
*  Copyright (c) Codiad, distributed
*  as-is and without warranty under the MIT License. See
*  [root]/license.txt for more. This information must remain intact.
*/

require_once('../../common.php');

//////////////////////////////////////////////////////////////////
// Verify Session or Key
//////////////////////////////////////////////////////////////////

checkSession();

switch($_GET['action']){

	//////////////////////////////////////////////////////////////////////
	// Show Warning
	//////////////////////////////////////////////////////////////////////
	
	case 'connection_lost':
		?>
		<form>
			<label>Codiad Collaborative</label>
			<p>
				The connection was lost, this page will reload in 5 seconds in an attempt to reconnect to the collaboration session.
			</p>
			<!--<button onclick="codiad.modal.unload(); return false;">Close</button>-->
		</form>
		<?php
	break;
	
	case 'joining':
		?>
		<form>
			<label>Codiad Collaborative</label>
			<p>
				Someone is joining the session please wait ...<br/>
				If this window stays open for longer than expected please refresh your page.
			</p>
			<!--<button onclick="codiad.modal.unload(); return false;">Close</button>-->
		</form>
		<?php
	break;
	
	case 'message':
		?>
		<form>
			<label>Codiad Collaborative</label>
			<pre><?php echo $_GET["message"];?></pre>
			<!--<em class="note">Note: This plugin requires at least Codiad 2.x.</em>--><br><br>
			<button onclick="codiad.modal.unload(); return false;">Close</button>
		</form>
		<?php
	break;
	
	case 'warn':
		
		?>
		<form>
			<label>Codiad Collaborative</label>
			<pre>Not compatible with this version of Codiad</pre>
			<em class="note">Note: This plugin requires at least Codiad 2.x.</em><br><br>
			<button onclick="codiad.modal.unload();return false;">Close</button>
		</form>
		<?php
	break;



}

?>