/**
*  Copyright (c) Codiad (codiad.com) & Isaac Brown (telaaedifex.com),
*  distributed as-is and without warranty under the MIT License. See
*  [root]/license.txt for more. This information must remain intact.
*/

( function( global, $ ) {
	
	// Define core
	var codiad = global.codiad,
	scripts= document.getElementsByTagName('script'),
	path = scripts[scripts.length-1].src.split('?')[0],
	curpath = path.split('/').slice(0, -1).join('/')+'/';
	
	$(function() {
		
		$.getJSON( curpath + 'config.json', function( result ) {
			
			codiad.collaborative.config = result;
			$.getScript( curpath + 'socket.io.js', function( result ) {
			
				codiad.collaborative.io = result;
				codiad.collaborative.initial();
			});
		});
	});
	
	//////////////////////////////////////////////////////////////////
	//
	// Collaborative Component for Codiad
	// ---------------------------------
	// Displays in real time
	// the changes when concurrently editing files.
	//
	//////////////////////////////////////////////////////////////////
	
	codiad.collaborative = {
		
		controller: curpath + 'controller.php',
		dialog: curpath + 'dialog.php',
		
		applying: false,
		changes: null,
		change_interval: null,
		current_editor: null,
		/**
		editors: {
			`file path`: {
				buffer: {
					element: null,
					last_applied_change: null,
					session: null,
				},
				`cached_changes`: [],
				`editor`: editor_object,
				`last_applied_change`: null,
				`session`: null,
				`track_changes`: false,
			}
		}
		*/
		editors: {},
		io: null,
		project: "",
		screen: {
			modal: null,
			loading: null,
			message: null,
		},
		socket: null,
		track_changes: false,
		verbose: true,
		
		initial: async function() {
			
			if( this.verbose ) {
				
				console.log( 'Contacting Collaborative Server' );
			}
			
			this.collaborative_server = `${this.config.server}:${this.config.port}`;
			this.socket = io.connect( this.collaborative_server, {
				'forceNew': true,
			});
			
			let connected = await this.check_connection();
			
			if( this.verbose ) {
				
				console.log( 'Connection status', connected );
			}
			
			if( ! connected ) {
				
				codiad.collaborative.socket.disconnect();
				codiad.modal.load( 500, codiad.collaborative.dialog + '?action=message&message=Error contacting collaborative server collaboration will be disabled for this session.<br>To attempt to reconnect, reload this page.' );
				throw new Error( "Error contacting Collaborative server." );
			}
			
			this.project = codiad.system.site_id + codiad.project.getCurrent();
			this.socket.emit( "set_project", this.project );
			this.register_socket_listeners();
			this.subscribers();
		},
		
		apply_changes: function( file, buffer=false ) {
			
			let _this = codiad.collaborative;
			let _file = _this.editors[file];
			let _buffer = _file.buffer;
			let _editor = _file.editor;
			let _session = _file.session;
			let done = 0;
			if( _this.applying ) {
				
				return;
			}
			
			_this.applying = true;
			let deltas = _file.cached_changes;
			let deltas_length = deltas.length;
			let width = 0;
			let loading_deltas = {};
			
			/**
			 * If buffer is true, use the editor in the background to apply the
			 * changes.  This allows for the user to not have their file blanked
			 * out while we loop through the deltas the server has for the file.
			 */
			
			if( buffer ) {
				
				loading_deltas = $().toastmessage( 'showToast', {
					text: 'Loading Deltas 0%',
					sticky: true,
					position: 'top-right',
					type: 'warning',
				});
				console.log( "Loading Buffer ... ", buffer, Date.now() );
				_buffer.session.setValue( "" );
			}
			
			/**
			 * When applying changes we check to see if the current file is the correct file for
			 * for this change, then we iterate through all changes.  During this operation we
			 * must set the last applied change before we make the change otherwise this value
			 * will be incorrect by the time the change event fires causing an infinate loop of
			 * the same change being applied to the document.  After we are finished iterating
			 * through all of the changes, we delete the changes we just iterated over.  We
			 * do not delete or reset the whole array in case new changes were added after we
			 * took the total amount of changes ( deltas_length ).
			 */
			
			if( ( codiad.system.site_id + _editor.getSession().path ) === file ) {
				
				for( i = 0; i < deltas_length; i++ ) {
					
					_file.last_applied_change = deltas[i];
					try {
						
						if( _this.verbose ) {
							
							console.log( deltas[i] );
						}
						
						if( buffer ) {
							
							//Apply the changes to the buffer.
							_buffer.session.getDocument().applyDelta( deltas[i] );
							done = ( ( ( i + 1 ) * 100 ) / deltas_length );
							loading_deltas.text( `Loading Deltas ${done}%` );
							if( _this.verbose ) {
								
								console.log( `Loading Deltas ${done}%` );
							}
						} else {
							
							//Apply the changes directly to the file.
							_session.getDocument().applyDelta( deltas[i] );
						}
					} catch( e ) {
						
						if( _this.verbose ) {
							
							console.log( "Error applying delta", deltas[i], e );
						}
					}
				}
				
				deltas.splice( 0, deltas_length );
				if( buffer ) {
					
					//Apply the changes from the buffer.
					let buffer_content = _buffer.session.getValue();
					_this.track_changes = false;
					_session.setValue( buffer_content );
					console.log( "Finished with Buffer ... ", buffer_content, Date.now() );
					_this.track_changes = true;
				}
			}
			
			if( buffer ) {
				
				setTimeout( function() {
					
					$().toastmessage( 'removeToast', loading_deltas );
				}, 3000 );
			}
			_this.applying = false;
		},
		
		change: function( event ) {
			
			let _this = codiad.collaborative;
			let _file = _this.editors[_this.current_editor];
			
			if( _this.change_interval !== null ) {
				
				clearTimeout( _this.change_interval );
				_this.change_interval = null;
			}
			_this.change_interval = setTimeout( _this.check_changes, 5000, _this.current_editor );
			
			if( ! _file.track_changes || ! _this.track_changes ) {
				
				if( _this.verbose ) {
					
					console.log( 'Change tracking is off, ignoring change.' );
				}
				_file.last_applied_change = event;
				return;
			}
			
			if( _file.last_applied_change != event ) {
				
				if( _this.verbose ) {
					
					console.log( _file.last_applied_change, event );
				}
				_this.socket.emit( "change", _this.current_editor, event );
				_file.last_applied_change = event;
			}
		},
		
		change_buffer: function( event, editor ) {
			
			let _this = codiad.collaborative
			let session = editor.getSession();
			let file = session.path;
			let _buffer = _this.editors[file].buffer;
			
			if( _buffer.last_applied_change != event ) {
				
				_buffer.last_applied_change = event;
			}
		},
		
		load_applied_changes: function( file, deltas ) {
			
			return new Promise( function( resolve, reject ) {
				
				let deltas_length = deltas.length;
				let _this = codiad.collaborative;
				let _buffer = _this.editors[file].buffer;
				_buffer.session.setValue( "" );
				
				for( i = 0; i < deltas_length; i++ ) {
					
					try {
						
						//Apply the changes to the buffer.
						_buffer.session.getDocument().applyDelta( deltas[i] );
					} catch( e ) {
						
						if( _this.verbose ) {
							
							console.log( "Error applying delta", deltas[i], e );
						}
					}
				}
				resolve( _buffer.session.getValue() );
			});
		},
		
		check_changes: async function( file ) {
			
			let _this = codiad.collaborative;
			let _file = _this.editors[file];
			
			if( _this.editors[file] == undefined || _this.editors[file].editor == undefined ) {
				
				return;
			}
			
			if( ! ( file === _this.current_editor ) ) {
				
				return;
			}
			console.log( "checking changes" );
			
			let _editor = _file.editor;
			let remote_changes = await _this.get_changes( file );
			let change_check = await _this.load_applied_changes( file, remote_changes );
			let content = _editor.getSession().getValue();
			
			if( ! ( change_check === content ) ) {
				
				console.log( "Re-Syncing File", ( change_check === _editor.getSession().getValue() ) );
				codiad.message.warning( 'Re-Syncing File' );
				
				_this.lock_editor( file );
				_this.editors[file].cached_changes = await _this.get_changes( file );
				_this.apply_changes( file, true );
				_this.unlock_editor( file );
			}
		},
		
		check_connection: function() {
			
			return new Promise( function( resolve, reject ) {
				
				function check( attempt = 0 ) {
					
					setTimeout( function() {
						
						let _this = codiad.collaborative;
						/**
						 * Check and see if we are connected to the collaboration server.
						 */
						
						if( ! _this.socket.connected ) {
							
							if( attempt >= 5 ) {
								
								resolve( false );
							} else {
								
								attempt++;
								console.log( attempt );
								check( attempt );
							}
						} else {
							
							resolve( true );
						}
					}, 512 );
				}
				
				check();
			});
		},
		
		click: function( event ) {
			
			let _this = codiad.collaborative;
			let _file = _this.editors[_this.current_editor];
		},
		
		close_file: function( file ) {
			
			let _this = codiad.collaborative;
			let _file = _this.editors[file];
			
			_this.current_editor = null;
			
			if( _this.verbose ) {
				
				console.log( 'Attempting to close file', file );
			}
			
			if( ! _file ) {
				
				if( _this.verbose ) {
					
					console.log( 'File is invalid, can not close file' );
				}
				return;
			}
			
			let _editor = _file.editor;
			
			if( ! _this.socket ) {
				
				if( _this.verbose ) {
					
					console.log( 'Socket is invalid, can not close file' );
				}
				return;
			} else {
				
				_this.socket.emit( "close_file", file );
			}
			
			if( _editor ) {
				
				_editor.removeEventListener( "change", _this.change );
			}
			
			if( _this.verbose ) {
					
				console.log( 'Removing file from array.' );
			}
			delete _this.editors[file];
		},
		
		get_changes: function( file ) {
			
			return new Promise( function( resolve, reject ) {
				
				let _this = codiad.collaborative;
				_this.socket.emit( "get_changes", file, function( i ) {
					
					resolve( i );
				});
			});
		},
		
		initialize_file: function( path ) {
			
			console.log( "focus", Date.now() );
			//codiad.message.success( "focus" );
			let file = codiad.system.site_id + path;
			let _this = codiad.collaborative;
			
			/**
			 * When an editor becomes focused we check to see if it is already the
			 * current editor to allow users to switch browser tabs without having to
			 * reload content, then we check and see if we already have the editor
			 * in our array of editors.  If we do then we just re-setup the listeners
			 * and refresh the editor.
			 */
			
			if( file === _this.current_editor ) {
				
				return;
			}
			
			codiad.message.warning( "Joining a collaborative session, please wait ..." );
			
			if( _this.change_interval !== null ) {
				
				clearInterval( _this.change_interval );
				_this.change_interval = null;
			}
			
			if( ! _this.editors[file] || ! _this.editors[file].editor ) {
				
				_this.open_file( file );
			} else {
				
				let	_file = _this.editors[file];
				let _editor = _file.editor;
				_this.current_editor = file;
				let content = _editor.getSession().getValue();
				_file.track_changes = false;
				
				for( let i = codiad.editor.instances.length; i--; ) {
					
					let editor = codiad.editor.instances[i];
					
					if( ( codiad.system.site_id + editor.getSession().path ) === file ) {
						
						_file.editor = editor;
						break;
					}
				}
				
				//_file.editor.setValue( content );
				//_file.editor.clearSelection();
				_file.track_changes = true;
				_this.apply_changes( file );
				_editor.addEventListener( "change", _this.change );
				_editor.addEventListener( "click", _this.click );
				_this.unlock_editor( file );
			}
			
			_this.change_interval = setTimeout( _this.check_changes, 2000, file );
		},
		
		load_content: function( file, deltas, callback, editor ) {
			
			//file = codiad.system.site_id + path;
			let _this = codiad.collaborative;
			let _file = _this.editors[file];
			let _editor = _file.editor;
			let _session = _file.session;
			let _content = _file.content;
			
			if( _this.verbose ) {
				
				console.log( "Loading content of " + file );
			}
			
			if( ! editor ) {
				
				if( _this.verbose ) {
					
					console.log( `Editor was ${editor} when attempting to make a change to ${file}` );
				}
				return;
			}
			
			if( _this.verbose ) {
				
				console.log( "Deltas:" );
				console.log( deltas );
			}
			
			let deltas_length = deltas.length;
			
			if( ! deltas_length ) {
				
				if( _this.verbose ) {
					
					console.log( "No deltas setting content from filesystem." );
				}
				
				_file.track_changes = true;
				_this.track_changes = true;
				_file.buffer.session.setValue( _content );
			} else {
				
				_file.cached_changes = deltas;
				_this.apply_changes( file, true );
			}
			
			_this.unlock_editor( file );
			callback( _file.buffer.last_applied_change );
		},
		
		lock_editor: function( file ) {
			
			let _this = codiad.collaborative;
			let _file = _this.editors[file];
			let _editor = _file.editor;
			
			codiad.editor.disableEditing( _editor );
		},
		
		open_file: function( file ) {
			
			let _this = codiad.collaborative;
			_this.editors[`${file}`] = {
				buffer: {
					element: null,
					editor: null,
					last_applied_change: null,
					session: null,
				},
				cached_changes: [],
				editor: null,
				session: null,
				last_applied_change: null,
				track_changes: false,
			};
			let _file = _this.editors[file];
			let _buffer = _file.buffer;
			let _editor = _file.editor;
			let _session = _file.session;
			
			/**
			 * Create buffer editor hidden on the page so that we can apply the
			 * deltas in the background without clearing out the users editor.
			 * This should allow the user to have a nicer experience and stop
			 * the file from saving blanked out if the user closes too early.
			 */
			_buffer.element = document.createElement("div");
			_buffer.element.style.display = "none";
			document.body.appendChild( _buffer.element );
			
			_buffer.editor = ace.edit( _buffer.element );
			_buffer.session = _buffer.editor.getSession();
			_buffer.session.path = file;
			_buffer.editor.addEventListener( "change", _this.change_buffer );
			
			if( _this.verbose ) {
				
				console.log( `Opening ` + file );
			}
			
			//Check and make sure the collaboration server is contactable before triggering collaboration.
			if( _this.socket.connected === false ) {
				
				if( _this.verbose ) {
					
					console.log( `Socket is not connected, stopping.` );
				}
				_this.track_changes = false;
				return;
			}
			
			for( let i = codiad.editor.instances.length; i--; ) {
				
				if( ( codiad.system.site_id + codiad.editor.instances[i].getSession().path ) === file ) {
					
					_this.editors[`${file}`].editor = codiad.editor.instances[i];
					_this.editors[`${file}`].session = _this.editors[`${file}`].editor.getSession();
					_editor = _this.editors[`${file}`].editor;
					_session = _this.editors[`${file}`].session;
					break;
				}
			}
			_this.track_changes = false;
			_this.current_editor = file;
			_file.track_changes = false;
			if( _editor === null ) {
				
				if( _this.verbose ) {
					
					console.log( `Editor is null, stopping.` );
				}
				return;
			}
			
			let _content = _session.getValue();
			_file.content = _content;
			_this.lock_editor( file );
			_this.socket.emit( "open_file", file );
			
			//Registering change callback
			_editor.addEventListener( "change", _this.change );
			_editor.addEventListener( "click", _this.click );
			_editor.$blockScrolling = Infinity;
		},
		
		register_socket_listeners: function() {
			
			this.socket.on( 'connect_failed', function() {
				
				codiad.active.removeAll();
				codiad.modal.load( 500, codiad.collaborative.dialog + '?action=connection_lost' );
				codiad.collaborative.track_changes = false;
				
				if( _this.verbose ) {
					
					console.log('Connection Failed');
				}
			});
			
			this.socket.on( 'disconnect', async function() {
				
				let _this = codiad.collaborative;
				let connected = await _this.check_connection();
				
				if( ! connected ) {
					
					codiad.active.removeAll();
					setTimeout( function() {
						
						window.location.reload();
					}, 2000);
					
					codiad.modal.load( 500, _this.dialog + '?action=connection_lost' );
					_this.track_changes = false;
					
					if( _this.verbose ) {
						
						console.log('Connection Failed');
					}
				}
			});
			
			this.socket.on( 'message', function( message ) {
				
				codiad.modal.load( 500, codiad.collaborative.dialog + '?action=message&message=' + message );
			});
			
			this.socket.on( 'change', function( file, delta ) {
				
				let _this = codiad.collaborative;
				let _file = _this.editors[file];
				
				if( _this.verbose ) {
					
					console.log( `Recieving change`, file, delta );
				}
				
				if( ! _file ) {
					
					if( _this.verbose ) {
						
						console.log( `File was ${_editor} when attempting to make a change to ${file}` );
					}
					return;
				}
				
				_file.cached_changes.push( delta );
				
				/**
				 * Check if the current editor is one of the editors that is open and
				 * visible.  If it is, then apply the changes right now.  If it is not
				 * then store the changes for later application.
				 */
				
				for( let i = codiad.editor.instances.length; i--; ) {
					
					let editor = codiad.editor.instances[i];
					
					if( ( codiad.system.site_id + editor.getSession().path ) === file ) {
						
						_this.apply_changes( file );
						break;
					}
				}
			});
			
			this.socket.on( 'load_content', function( file, deltas, callback ) {
				
				if( codiad.collaborative.verbose ) {
					
					console.log( 'Someone is joining, freezing editor ...' );
				}
				
				let _this = codiad.collaborative;
				let _file = _this.editors[file];
				let _editor = _file.editor;
				_this.track_changes = false;
				
				codiad.editor.disableEditing( _editor );
				
				if( ( codiad.system.site_id + _editor.getSession().path ) === file ) {
					
					_this.load_content( file, deltas, callback, _editor );
				}
			});
			
			this.socket.on( 'lock', function( file ) {
				
				let _this = codiad.collaborative;
				_this.lock_editor( file );
			});
			
			this.socket.on( 'refresh_project', function( path ) {
				
				let _this = codiad.collaborative;
				if( _this.verbose ) {
					
					console.log( 'Rescanning path ...' );
				}
				
				codiad.filemanager.rescan( codiad.project.getCurrent() );
			});
			
			this.socket.on( 'unlock', function( file ) {
				
				let _this = codiad.collaborative;
				if( _this.verbose ) {
					
					console.log( 'Unfreezing editor ...' );
				}
				
				_this.unlock_editor( file );
			});
		},
		
		subscribers: function() {
			
			amplify.subscribe( 'filemanager.onFileWillOpen', function( path ) {
				
				//.log( "fopen", Date.now() );
				//codiad.message.success( "fopen" );
				let _this = codiad.collaborative;
				//_this.screen.loading.style.width = 0;
				//_this.screen.message.innerText = "Calibrating Collaborative Session";
				//_this.screen.modal.display = "block";
			});
			
			amplify.subscribe( 'active.onFileWillAppear', function( path ) {
				
				//console.log( "willappear", Date.now() );
				//codiad.message.success( "willappear" );
			});
			
			amplify.subscribe( 'active.onOpen', function( path ) {
				
				//console.log( "open", Date.now() );
				//codiad.message.success( "open" );
			});
			
			/* Subscribe to know when a file is being closed. */
			amplify.subscribe( 'active.onClose', function( path ) {
				
				let file = codiad.system.site_id + path;
				let _this = codiad.collaborative;
				
				if( _this.change_interval !== null ) {
					
					clearTimeout( _this.change_interval );
					_this.change_interval = null;
				}
				
				_this.close_file( file );
			});
			
			/* Subscribe to know when a file become active. */
			amplify.subscribe( 'active.onFocus', function( path ) {
				
				codiad.collaborative.initialize_file( path );
			});
			
			amplify.subscribe( 'active.onRemoveAll', function() {
				
				console.log( "Closing all editors." );
				let _this = codiad.collaborative;
				let keys = Object.keys( _this.editors );
				let keys_length = keys.length;
				
				for( let i = 0;i < keys_length; i++ ) {
					
					console.log( "Closing file: " + keys[i] );
					_this.close_file( keys[i] );
				}
			});
			
			amplify.subscribe( 'filemanager.onCreate', function( object ) {
				
				let _this = codiad.collaborative;
				
				if( _this.verbose ) {
					
					console.log( object );
				}
				
				_this.socket.emit( "refresh_project", _this.project );
			});
			
			amplify.subscribe( 'filemanager.onDelete', function( object ) {
				
				let _this = codiad.collaborative;
				
				if( _this.verbose ) {
					
					console.log( object );
				}
				
				_this.socket.emit( "refresh_project", _this.project );
			});
			
			amplify.subscribe( 'filemanager.onPaste', function( object ) {
				
				let _this = codiad.collaborative;
				
				if( _this.verbose ) {
					
					console.log( path );
				}
				
				_this.socket.emit( "refresh_project", _this.project );
			});
			
			amplify.subscribe( 'filemanager.onRename', function( object ) {
				
				let _this = codiad.collaborative;
				
				if( _this.verbose ) {
					
					console.log( path );
				}
				
				_this.socket.emit( "refresh_project", _this.project );
			});
			
			
			
			amplify.subscribe( 'project.onOpen', function( path ) {
				
				let _this = codiad.collaborative;
				_this.project = codiad.system.site_id + path;
				
				if( _this.verbose ) {
					
					console.log( path );
				}
				
				_this.socket.emit( "set_project", _this.project );
			});
			
		},
		
		unlock_editor: function( file ) {
			
			let _this = codiad.collaborative;
			
			if( _this.verbose ) {
					
				console.log( 'Unfreezing editor ...' );
			}
			
			if( _this.editors[file] && _this.editors[file].editor ) {
				
				let _file = _this.editors[file];
				let _editor = _file.editor;
				_file.track_changes = true;
				_this.track_changes = true;
				codiad.editor.enableEditing( _editor );
			}
		},
	};
})(this, jQuery);
